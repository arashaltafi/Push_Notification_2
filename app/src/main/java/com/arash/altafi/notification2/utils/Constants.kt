package com.arash.altafi.notification2.utils

object Constants {
    const val KEY_REPLY = "KEY_REPLAY"
    const val CHANNEL_ID = "CHANNEL_ID"
    const val CHANNEL_NAME = "CHANNEL_NAME"
    const val REPLY_KEY = "REPLY_KEY"
    const val NOTIFICATION_ID = 1
    const val BASE_URL = "https://fcm.googleapis.com"
    const val SERVER_KEY =
        "AAAAYUUCqJI:APA91bFgqidAAGsT7hhptyJLNr04k9ycJwGglsNxxN_3KHqdv32Qyptm_hzIlVeGRMWxXmewqmK8UeoAYxIc8j5stU0UMkSA2TE-UvuzuDW2XZ-Jxji9VCjfkxI0eC4_XGQoR6RjE6-i"
    const val CONTENT_TYPE = "application/json"
    const val EMULATOR_API_29_TOKEN =
        "eKuTp4VzSameMkEKac1zpK:APA91bENaTj1W4kcKfAMxSFFT4N2LAetVcyAXyIRuAzW6Gd4a6wql7FjowM_VkVb1GG-mlVxatGTQdMWl5sapu5f94Ht5vx7o1XYG7C6-EZB4v5ydw4uQFMDs-8Wt2u74UuodLOS0uOC"
    const val PHONE_S21_FE_TOKEN =
        "dbNtOcV8QFu8fUX1YeQd0l:APA91bEI3PHGdoBKOBBYpwXSHK19EubzzLbsQSiCrAl4YV5Ij3Advfx-zkB41piCV7TAATq3GVC0FRuEydSuNVa_LDXZjWffVzJPR0WgTE_MyIyNjwL77w1QwMmGlfFCFCacGD27VE7I"
}